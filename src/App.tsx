import { Gallery } from './components/Gallery';

// load gallery component, and create images array
export function App() {
  return (
    <div className="gallery">
      <Gallery
        photos={[
          {
            id: 1,
            src: '/photos/1.jpg',
            preview: '/photos/preview/1_mini.jpg',
            description: 'lorem-text',
          },
          {
            id: 2,
            src: '/photos/2.jpg',
            preview: '/photos/preview/2_mini.jpg',
            description: 'lorem-text',
          },
          {
            id: 3,
            src: '/photos/3.jpg',
            preview: '/photos/preview/3_mini.jpg',
            description: 'lorem-text',
          },
          {
            id: 4,
            src: '/photos/4.jpg',
            preview: '/photos/preview/4_mini.jpg',
            description: 'lorem-text',
          },
          {
            id: 5,
            src: '/photos/5.jpg',
            preview: '/photos/preview/5.jpg',
            description: 'empty image',
          },
        ]}
      />
    </div>
  );
}
