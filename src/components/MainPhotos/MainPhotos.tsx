import { FC, MutableRefObject, useLayoutEffect, useRef, useState } from 'react';
import cl from 'classnames';

import { ICommonClassProps, IPhoto } from '../../types';

import style from './mainPhotos.module.scss';

interface IMainPhotosProps extends ICommonClassProps {
  photos: IPhoto[];
  activePhotoIndex: number;
}

// getPhotoByRef
// get img-element[index] from images container
type RefT = MutableRefObject<HTMLDivElement | null>;
const getPhotoByRef = (ref: RefT, index: number): HTMLElement | null =>
  ref.current!.querySelector(`img:nth-of-type(${index + 1})`);

// show/hide img-element
const hidePhoto = (element: HTMLElement | null) => {
  if (!element) return;

  element.dataset.active = 'false';
  if (element.previousSibling) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    element.previousSibling.dataset.active = 'false';
  }
  if (element.nextSibling) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    element.nextSibling.dataset.active = 'false';
  }
};

const showPhoto = (element: HTMLElement | null) => {
  if (!element) return;

  element.dataset.active = 'true';
  if (element.previousSibling) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    element.previousSibling.dataset.active = 'prepared';
  }
  if (element.nextSibling) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    element.nextSibling.dataset.active = 'prepared';
  }
};

// MainPhoto is for viewing a large photo
export const MainPhotos: FC<IMainPhotosProps> = ({
  className,
  photos,
  activePhotoIndex,
}) => {
  // set a link to the images container
  const containerRef = useRef<HTMLDivElement | null>(null);

  // save the index of the previous image
  const [prevActivePhotoIndex, setPrevActivePhotoIndex] =
    useState(activePhotoIndex);

  // useLayoutEffect - это версия useEffect, которая срабатывает перед тем, как браузер перерисовывает экран (similar componentDidMount).
  useLayoutEffect(() => {
    if (!containerRef.current) return;

    const activePhoto = getPhotoByRef(containerRef, prevActivePhotoIndex);
    const nextPhoto = getPhotoByRef(containerRef, activePhotoIndex);

    if (prevActivePhotoIndex !== activePhotoIndex) {
      hidePhoto(activePhoto);
      showPhoto(nextPhoto);
    } else {
      showPhoto(activePhoto);
    }

    // set new active photo
    setPrevActivePhotoIndex(activePhotoIndex);
  }, [activePhotoIndex, prevActivePhotoIndex]);

  return (
    <div className={cl(className, style.mainPhotos)} ref={containerRef}>
      {photos.map((photo, id) => (
        <img
          key={photo.id}
          className={style.mainPhotoImage}
          src={photo.src}
          alt={photo.description}
          loading="lazy"
          data-active={id === activePhotoIndex}
        />
      ))}
    </div>
  );
};
