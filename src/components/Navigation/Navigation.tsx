import { FC } from 'react';
import cl from 'classnames';

import { ICommonClassProps } from '../../types';

import style from './navigation.module.scss';

interface INavigationProps extends ICommonClassProps {
  disabledPrev: boolean;
  disabledNext: boolean;
  onPrevClick: () => void;
  onNextClick: () => void;
}

// Navigation for scrolling images
export const Navigation: FC<INavigationProps> = ({
  className,
  disabledPrev,
  disabledNext,
  onPrevClick,
  onNextClick,
}) => {
  return (
    <div className={cl(className, style.navigation)}>
      <button
        disabled={disabledPrev}
        className={style.navigationBtn}
        onClick={onPrevClick}
      >
        {'<'}
      </button>
      <button
        disabled={disabledNext}
        className={style.navigationBtn}
        onClick={onNextClick}
      >
        {'>'}
      </button>
    </div>
  );
};
