import { useState } from 'react';
import { IPhoto } from '../types';
import { MainPhotos } from './MainPhotos/MainPhotos';
import { Navigation } from './Navigation/Navigation';
import { PreviewPhotos } from './PreviewPhotos/PreviewPhotos';

import style from './gallery.module.scss';

interface IGalleryProps {
  photos: IPhoto[];
}

// General gallery component use MainPhoto, PreviewPhoto, Navigation components
export const Gallery: React.FC<IGalleryProps> = ({ photos }) => {
  const [indexActivePhoto, setIndexActivePhoto] = useState(0);

  const prevPhoto = photos[indexActivePhoto - 1];
  const nextPhoto = photos[indexActivePhoto + 1];

  if (!photos.length) {
    return null;
  }
  return (
    <div className={style.Gallery}>
      <div className={style.GalleryContainer}>
        <MainPhotos
          className={style.GalleryMainPhotos}
          // prevPhoto={prevPhoto}
          // currentPhoto={currentPhoto}
          // nextPhoto={nextPhoto}
          photos={photos}
          activePhotoIndex={indexActivePhoto}
        />
        <Navigation
          className={style.GalleryNavigation}
          disabledPrev={!prevPhoto}
          disabledNext={!nextPhoto}
          onPrevClick={() => {
            setIndexActivePhoto(indexActivePhoto - 1);
          }}
          onNextClick={() => {
            setIndexActivePhoto(indexActivePhoto + 1);
          }}
        />
      </div>
      <PreviewPhotos
        className={style.GalleryPreviewPhotos}
        activePhotoIndex={indexActivePhoto}
        photos={photos}
        setNewPhoto={setIndexActivePhoto}
      />
    </div>
  );
};
