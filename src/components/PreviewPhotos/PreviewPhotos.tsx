import { FC, useEffect, useRef } from 'react';
import cl from 'classnames';

import { ICommonClassProps, IPhoto } from '../../types';

import style from './previewPhotos.module.scss';

interface IPreviewPhotosProps extends ICommonClassProps {
  activePhotoIndex: number;
  setNewPhoto: (id: number) => void;
  photos: IPhoto[];
}

// PreviewPhotos is for viewing small photos
export const PreviewPhotos: FC<IPreviewPhotosProps> = ({
  className,
  activePhotoIndex,
  photos,
  setNewPhoto,
}) => {
  const previewContainer = useRef<HTMLUListElement>(null);

  useEffect(() => {
    if (!previewContainer.current) {
      return;
    }
    previewContainer.current.style.transform = `translateX(-${
      activePhotoIndex * 160
    }px)`;
  }, [activePhotoIndex]);

  return (
    <div className={cl(className, style.previewPhotos)}>
      <ul className={style.previewPhotosTrack} ref={previewContainer}>
        {photos.map((photo, id) => (
          <li key={photo.id} className={style.previewPhotosPreview}>
            <button
              className={style.previewPhotosBtn}
              onClick={() => setNewPhoto(id)}
            >
              <img
                className={style.previewPhotosImage}
                src={photo.preview}
                alt={photo.description}
              />
            </button>
          </li>
        ))}
      </ul>
      <div className={style.previewPhotosCover}>
        {activePhotoIndex + 1}/{photos.length}
      </div>
    </div>
  );
};
